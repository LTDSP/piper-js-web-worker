/**
 * Created by lucast on 14/10/2016.
 */
importScripts("node_modules/piper/ext/VampExamplePlugins.js");
importScripts("node_modules/piper/dist/piper.js");

const vamp = new Piper.EmscriptenProxy(VampExamplePluginsModule());

const router = {
    list: (request) => {
        vamp.list(request).then(resp => {
            postMessage({method: "list", result: resp});
        }).catch(reportError);
    },
    load: (request) => {
        vamp.load(request.params).then(resp => {
            postMessage({method: "load", result: resp});
        }).catch(reportError);
    },
    configure: (request) => {
        vamp.configure(request.params).then(resp => {
            postMessage({method: "configure", result: resp});
        }).catch(reportError);
    },
    process: (request) => {
        vamp.process(request.params).then(resp => {
            postMessage({method: "process", result: resp});
        }).catch(reportError);
    },
    finish: (request) => {
        vamp.finish(request.params).then(resp => {
            postMessage({method: "finish", result: resp});
        }).catch(reportError);
    }
};

onmessage = (message) => {
    if (message.data instanceof Object
        && message.data.hasOwnProperty("method")
        && message.data.hasOwnProperty("params")
    ) {
        router[message.data.method](message.data);
    } else {
        reportError(message.data);
    }
};


function reportError (err) {
    console.error(err);
}
